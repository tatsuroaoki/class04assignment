// IN-CLASS EXERCISE #1: FOOD TEMPERATURE EXERCISE

const foodIsCooked = function (kind, internalTemp, doneness) {
	if (kind === 'chicken' && internalTemp >= 165) {
		return true;
	} else if (kind === 'beef' && internalTemp >= 125 && doneness === 'rare') {
		return true;
	} else if (kind === 'beef' && internalTemp >= 135 && doneness === 'medium') {
		return true;
	} else if (kind === 'beef' && internalTemp >= 155 && doneness === 'well') {
		return true;
	} else {
		return false;
	}
}

// Check
console.log(foodIsCooked('beef', 124, 'rare'));

// IN-CLASS EXERCISE #2: CREATE A DECK OF CARDS

const suits = ['spades', 'hearts', 'clubs','diamonds'];

// Function that pushes 52 card objects to the cards array
const getDeck = function() {
	const cards = [];

	// Loops through suits
	for (let i = 0; i <= suits.length -1; i++) {
		// Loops through cards 2 through 10 for a given suit
		for (let j = 2; j <= 10; j++) {
			cards.push({
				val: j,
				displayValue: j.toString(),
				suit: suits[i]
			});
		}
		// Loops through face cards for a given suit
		cards.push({
			val: 10,
			displayValue: 'jack',
			suit: suits[i]
		});
		cards.push({
			val: 10,
			displayValue: 'queen',
			suit: suits[i]
		});
		cards.push({
			val: 10,
			displayValue: 'king',
			suit: suits[i]
		});
		cards.push({
			val: 11,
			displayValue: 'ace',
			suit: suits[i]
		});
	}
	return cards;
}

// HOMEWORK: BLACKJACK

// Step A
const blackjackDeck = getDeck();

// Step B
const CardPlayer = function CardPlayer(name) {
	this.name = name;
	this.hand = [];
	this.drawCard = function() {
		let randomCardSelection = Math.ceil(Math.random() * blackjackDeck.length) - 1;
		this.hand.push(blackjackDeck[randomCardSelection]);
		return this.hand;
	};
};

// Step C
const player = new CardPlayer('Jason Bourne');
const dealer = new CardPlayer('Joker');

// Step D

const calcPoints = function(hand) {
	this.total = 0;
	this.isSoft = false;

	for (let card in hand) {
		if ((this.total + hand[card].val) > 21 && hand[card].displayValue === 'ace') {
			hand[card].val = 1;
		} else if (this.total <= 21 && hand[card].val === 11) {
			this.isSoft = true;
		}
		this.total += hand[card].val;
	}
	return this;
}

// STEP E

const dealerShouldDraw = function(dealerHand) {
	this.dealerTotal = 0;
	let hasAce = false;
	for (card in dealerHand) {
		if (dealerHand[card].displayValue === 'ace') {
			hasAce = true;
		}
		this.dealerTotal += dealerHand[card].val;
	}

	if (this.dealerTotal <= 16 || (this.dealerTotal === 17 && hasAce === true)) {
		return true;
	} else {
		return false;
	}
}

// STEP F
const determineWinner = function(pScore, dScore) {
	if (pScore > dScore) {
		return `Player scored ${pScore} points. Dealer scored ${dScore} points. Player wins!`
	} else if (pScore < dScore) {
		return `Player scored ${pScore} points. Dealer scored ${dScore} points. Dealer wins!`
	} else {
		return `Player scored ${pScore} points. Dealer scored ${dScore} points. It's a split!`;
	}
}

const getMessage = function(count, dealerCard) {
	return `Dealer showing ${dealerCard.displayValue}, your count is ${count}.  Draw card?`
}

const showHand = function(player) {
	let displayHand = player.hand.map(function(card) { return card.displayValue});
	console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

const startGame = function() {
	// Initial draw
	player.drawCard();
	dealer.drawCard();
	player.drawCard();
	dealer.drawCard();

	// Player Logic
	let playerScore = calcPoints(player.hand).total;
	showHand(player);

	while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
		player.drawCard();
		playerScore = calcPoints(player.hand).total;
		showHand(player);
	}

	if (playerScore > 21) {
		return `${player.name} went over 21 - ${player.name} loses!`;
	} else {
		console.log(`${player.name} stands at ${playerScore}`);
	}

	// Dealer Logic
	let dealerScore = calcPoints(dealer.hand).total;
	showHand(dealer);
	while (dealerShouldDraw(dealer.hand) === true) {
		dealer.drawCard();
		dealerScore = calcPoints(dealer.hand).total;
		showHand(dealer);
	}

	if (dealerScore > 21) {
		return `${dealer.name} went over 21 - ${player.name} wins!`;
	} else {
		console.log(`${dealer.name} stands at ${dealerScore}`);
	}
	return determineWinner(playerScore,dealerScore);
}

console.log(startGame());